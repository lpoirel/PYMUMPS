from mpi4py import MPI
from pymumps import Mumps
import scipy.sparse as spp
import numpy as np

# Initialize mumps wrapper
driver_mumps = Mumps('D')
# Get corresponding numpy type
nptype = driver_mumps.nptype

# MPI stuff
comm = MPI.COMM_WORLD
myid = comm.Get_rank()
nprocs = comm.Get_size()

# Set fortran communicator
id = driver_mumps.id
id.comm_fortran = comm.py2f()

# Set par to 1 for sequential version
if nprocs == 1:
    id.par = 1

# Init mumps
driver_mumps.initialize()

# Set matrix A
n = 9
row = np.array([0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8])
col = np.array([0, 1, 3, 0, 1, 2, 4, 1, 2, 5, 0, 3, 4, 6, 1, 3, 4, 5, 7, 2, 4, 5, 8, 3, 6, 7, 4, 6, 7, 8, 5, 7, 8])
data = np.array([4.0, -1.0, -1.0, -1.0, 4.0, -1.0, -1.0, -1.0, 4.0, -1.0, -1.0, 4.0, -1.0, -1.0, -1.0, -1.0, 4.0,
                 -1.0, -1.0, -1.0, -1.0, 4.0, -1.0, -1.0, 4.0, -1.0, -1.0, -1.0, 4.0, -1.0, -1.0, -1.0, 4.0], dtype=nptype)

NRHS = 2
if driver_mumps.is_complex:
    data.imag += 1
A = spp.coo_matrix((data, (row, col)), shape=(n, n))

th_sol = np.zeros((n, NRHS,), dtype=nptype)
for i in range(NRHS):
    k = i * NRHS
    if driver_mumps.is_complex:
        th_sol[:,i].real = np.arange(k+1.0, k+n+1.0)
        th_sol[:,i].imag = np.arange(-n-k, -k)
    else:
        th_sol[:,i] = np.arange(k+1.0, k+n+1.0, dtype=nptype)

# Set rhs
rhs = np.matmul(A.todense(), th_sol)

if myid == 0:
    driver_mumps.set_A(A)
    driver_mumps.set_RHS(rhs)

# Set icntl
driver_mumps.ICNTL[1] = 6
driver_mumps.ICNTL[2] = 6
driver_mumps.ICNTL[3] = 6
driver_mumps.ICNTL[4] = 6

# Call mumps
driver_mumps.drive(6)

if myid == 0:
    sol = driver_mumps.get_solution()
    print("Found:")
    print(sol)
    print("Th. solution:")
    print(th_sol)

# Finalize mumps
driver_mumps.finalize()

if myid == 0:
    print("done")
