"""
PyMUMPS
"""
from ctypes import *
from ctypes.util import find_library
import os

__authors__ = "Gilles Marait"
__versions__ = "0.1"

dir_path = os.path.dirname(os.path.realpath(__file__))

ld_libpath = ""
if 'LD_LIBRARY_PATH' in os.environ.keys():
    ld_libpath = os.environ['LD_LIBRARY_PATH']
    ld_libpath = ld_libpath.split(':')

def search_ld_libpath(libname):
    fullname = 'lib' + libname + '.so'
    for libdir in ld_libpath:
        fullpath = os.path.join(libdir, fullname)
        if os.path.isfile(fullpath):
            return fullpath



# Mumps dependencies
# To load before the mumps libraries
deps = [
    'scalapack',
    'blas',
    'scotcherr',
    'scotch',
    'ptscotch',
    'esmumps',
    'metis',
    'pord',
    'mumps_common'
]

# Mumps libraries to find
xmumps = [
    'cmumps',
    'dmumps',
    'smumps',
    'zmumps'
]

# If no cache yet, write empty cache
if not os.path.isfile(os.path.join(dir_path, "libcache.py")):
    # Write in the cache the path of the found libraries
    with open(os.path.join(dir_path, "libcache.py"), 'w') as f:
        f.write(r"libs_cache = {" + "\n")
        for lib in deps:
            f.write('    "' + str(lib) + '" : "",\n')
        for lib in xmumps:
            f.write('    "' + str(lib) + '" : "",\n')
        f.write("}\n")

from .libcache import libs_cache

# Find and load dependencies
for libname in deps:

    # First try to get the path from the cache
    if libs_cache[libname]:
        try:
            CDLL(libs_cache[libname], mode=RTLD_GLOBAL)
            continue
        except Exception:
            print("Error loading" + str(libs_cache[libname]))

    # Otherwise try to find the library manually
    lib = find_library(libname)

    if not lib:
        lib = search_ld_libpath(libname)

    if lib:
        CDLL(lib, mode=RTLD_GLOBAL)
    else:
        print("Try to specify the library paths in: " + os.path.join(dir_path, "libcache.py"))
        raise EnvironmentError("Could not find shared library: " + libname)

    libs_cache[libname] = lib

# Find xmumps libraries
for libname in xmumps:
    if not libs_cache[libname]:
        lib = find_library(libname)
        if lib:
            libs_cache[libname] = lib
        else:
            print("Try to specify the library paths in: " + os.path.join(dir_path, "libcache.py"))
            raise EnvironmentError("Could not find shared library: " + libname)

# Write in the cache the path of the found libraries
with open(os.path.join(dir_path, "libcache.py"), 'w') as f:
    f.write(r"libs_cache = {" + "\n")
    for lib in deps:
        f.write('    "' + str(lib) + '" : "' + str(libs_cache[lib]) + '",\n')
    for lib in xmumps:
        f.write('    "' + str(lib) + '" : "' + str(libs_cache[lib]) + '",\n')
    f.write("}\n")

from .Mumps import *

__all__ = ['Mumps']
